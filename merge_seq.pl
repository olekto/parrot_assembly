#!/usr/bin/perl

#This script merges the sequences from a fastq file so that the number of entries in the resulting
#fasta file is quite a bit less. Meryl doesn't like a lot of entries.

use strict;

my $len = 0;

print ">\n";

while (!eof(STDIN)) {
    my $a = <STDIN>;
    my $b = <STDIN>;
    my $c = <STDIN>;
    my $d = <STDIN>;

    chomp $b;

    print "${b}N";

    $len += length($b);

    if ($len > 1000000) {
        $len = 0;
        print "\n>\n";
    }
}

print "\n";
