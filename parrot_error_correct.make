parrot_220_1.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDCAAPE/110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_1.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDCAAPE/110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_1.fq.gz.md5
	md5sum -c 110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_1.fq.gz.md5
	mv 110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_1.fq.gz parrot_220_1.fq.gz
	gunzip parrot_220_1.fq.gz
	touch parrot_220_1.fq
	

parrot_220_2.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDCAAPE/110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_2.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDCAAPE/110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_2.fq.gz.md5
	md5sum -c 110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_2.fq.gz.md5
	mv 110428_I327_FCB00D2ACXX_L2_PARprgDAPDCAAPE_2.fq.gz parrot_220_2.fq.gz
	gunzip parrot_220_2.fq.gz 
	touch parrot_220_2.fq

	
parrot_500_1.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDIAAPE/110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_1.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDIAAPE/110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_1.fq.gz.md5
	md5sum -c 110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_1.fq.gz.md5
	mv 110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_1.fq.gz parrot_500_1.fq.gz
	gunzip parrot_500_1.fq.gz
	touch parrot_500_1.fq
	
parrot_500_2.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDIAAPE/110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_2.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDAPDIAAPE/110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_2.fq.gz.md5
	md5sum -c 110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_2.fq.gz.md5
	mv 110428_I327_FCB00D2ACXX_L3_PARprgDAPDIAAPE_2.fq.gz parrot_500_2.fq.gz
	gunzip parrot_500_2.fq.gz
	touch parrot_500_2.fq
	
parrot_5k_1_1.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLAAPE/110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_1.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLAAPE/110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_1.fq.gz.md5
	md5sum -c 110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_1.fq.gz.md5
	mv 110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_1.fq.gz parrot_5k_1_1.fq.gz
	gunzip parrot_5k_1_1.fq.gz
	touch parrot_5k_1_1.fq
	
parrot_5k_1_2.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLAAPE/110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_2.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLAAPE/110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_2.fq.gz.md5
	md5sum -c 110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_2.fq.gz.md5
	mv 110514_I247_FC81MVPABXX_L5_PARprgDABDLAAPE_2.fq.gz parrot_5k_1_2.fq.gz
	gunzip parrot_5k_1_2.fq.gz
	touch parrot_5k_1_2.fq
	
parrot_5k_2_1.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLBAPE/110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_1.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLBAPE/110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_1.fq.gz.md5
	md5sum -c 110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_1.fq.gz.md5
	mv 110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_1.fq.gz parrot_5k_2_1.fq.gz
	gunzip parrot_5k_2_1.fq.gz
	touch parrot_5k_2_1.fq
	
parrot_5k_2_2.fq: 
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLBAPE/110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_2.fq.gz
	wget http://bioshare.bioinformatics.ucdavis.edu/Data/hcbxz0i7kg/Parrot/BGI_illumina_data/PARprgDABDLBAPE/110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_2.fq.gz.md5
	md5sum -c 110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_2.fq.gz.md5
	mv 110514_I263_FC81PACABXX_L5_PARprgDABDLBAPE_2.fq.gz parrot_5k_2_2.fq.gz
	gunzip parrot_5k_2_2.fq.gz
	touch parrot_5k_2_2.fq
	
get_data: parrot_220_1.fq parrot_220_2.fq parrot_500_1.fq parrot_500_2.fq parrot_5k_1_1.fq parrot_5k_1_2.fq parrot_5k_2_1.fq parrot_5k_2_2.fq

trusted_kmers_pe.fa: get_data 
	#Only want to count kmers in the paired end data. Not sure if this is the optimal, but the mate pair probably have a
	#higher occurrence of error kmers.
	cat parrot_220_1.fq | perl merge_seq.pl > trusted_kmers_pe.fa
	cat parrot_220_2.fq | perl merge_seq.pl >> trusted_kmers_pe.fa
	cat parrot_500_1.fq | perl merge_seq.pl >> trusted_kmers_pe.fa
	cat parrot_500_2.fq | perl merge_seq.pl >> trusted_kmers_pe.fa
	touch trusted_kmers_pe.fa

trusted_kmers.mcdat: trusted_kmers_pe.fa
	#I assume that you have at least 32 cores here, and at least 250000 MB of RAM (bit less than 250 GB)
	#Also, I assume that the assembly is going to use 22 mers as seeds, and therefore correct those, but 
	#that might depend on the species
	#Need meryl in your path
	meryl -B -m 22 -memory 250000 -threads 32 -C -s trusted_kmers_pe.fa -o trusted_kmers

parrot_220.extendedFrags.fastq: get_data
	#Need flash in your path
	#150 bp long reads (-r), 220 bp fragments (-f), standard deviation of 22 (-s), 
	flash -r 150 -f 220 -s 22 parrot_220_1.fq parrot_220_2.fq -o parrot_220 > merged_parrot_220.out 2>&1

trim_the_fastq_files: trusted_kmers.mcdat parrot_220.extendedFrags.fastq
	#Set up an output directory
	mkdir -p trimmed_and_corrected_reads
	#Need merTrim in your path
	#export PATH=/projects/454data/bin/celera/wgs-June11_2013/Linux-amd64/bin:$PATH
	merTrim -F parrot_220.extendedFrags.fastq -m 22 -mc trusted_kmers -mCillumina -t 32 -o trimmed_and_corrected_reads/parrot_220.extendedFrags_mT.fq 1> parrot_220_ext_mT.out 2> parrot_220_ext_mT.err
